import { Routes } from "@angular/router";
import { ApplicationsTableComponent } from "./applications-table/applications-table.component";

export const appRoutes: Routes = [
    { path: 'applications-table', component: ApplicationsTableComponent }
];