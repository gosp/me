import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';

export interface TableItems {
  id: string;
  name: number;
  type: number;
  gateway: number;
}

@Component({
  selector: 'app-applications-table',
  templateUrl: './applications-table.component.html',
  styleUrls: ['./applications-table.component.css']
})
export class ApplicationsTableComponent implements OnInit {

  data: TableItems[] = [
    {id: 1, name: 'Website1', type: 0, gateway: 1},
    {id: 2, name: 'Helium', type: 2, gateway: 2},
    {id: 3, name: 'Lithium', type: 2, gateway: 1},
    {id: 4, name: 'Beryllium', type: 0, gateway: 2},
    {id: 5, name: 'Boron', type: 1, gateway: 1}
  ];

  typesFilter = [0, 1];
  gatewaysFilter = [1];

  displayedColumns: string[] = ['id', 'name', 'type', 'gateway'];
  dataSource = new MatTableDataSource(this.data);

  @ViewChild(MatSort) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  update() {
    this.dataSource.data = this.data.filter((item) => this.typesFilter.includes(item.type) && this.gatewaysFilter.includes(item.gateway));
  }

  constructor() { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

}
